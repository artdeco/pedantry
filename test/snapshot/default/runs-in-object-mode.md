# test/fixture/directory/index.md

```markdown
# Index.md<CR?LF>
<CR?LF>
anything is something worth discovering<CR?LF>
```
# separator

```markdown
<CR?LF>
<CR?LF>
```
# test/fixture/directory/earth.md

```markdown
<CR?LF>
# Developers Developers<CR?LF>
<CR?LF>
Developers Developers<CR?LF>
```
# separator

```markdown
<CR?LF>
<CR?LF>
```
# test/fixture/directory/file.md

```markdown
<CR?LF>
Hashtag OK Hueston.<CR?LF>
```
# separator

```markdown
<CR?LF>
<CR?LF>
```
# test/fixture/directory/new-directory/index.md

```markdown
<CR?LF>
# New Directory<CR?LF>
<CR?LF>
It is very exciting.<CR?LF>
```
# separator

```markdown
<CR?LF>
<CR?LF>
```
# test/fixture/directory/new-directory/ice.md

```markdown
<CR?LF>
## Cold as 🇦🇶 Ice<CR?LF>
<CR?LF>
Your heart.<CR?LF>
```
# separator

```markdown
<CR?LF>
<CR?LF>
```
# test/fixture/directory/new-directory/queen.md

```markdown
<CR?LF>
## wrote<CR?LF>
<CR?LF>
wrote to the queen about my heartbreak but she didn't answer<CR?LF>
```
# separator

```markdown
<CR?LF>
<CR?LF>
```
# test/fixture/directory/new-directory/water.md

```markdown
<CR?LF>
## This is 🚰 water.<CR?LF>
<CR?LF>
It is even older that @earth.<CR?LF>
```
# separator

```markdown
<CR?LF>
<CR?LF>
```
# test/fixture/directory/readme.md

```markdown
<CR?LF>
# Documentation is fascinating.<CR?LF>
<CR?LF>
Where will we be<CR?LF>
when the code is gone<CR?LF>
```
# separator

```markdown
<CR?LF>
<CR?LF>
```
# test/fixture/directory/footer.md

```markdown
<CR?LF>
---<CR?LF>
<CR?LF>
Kings of medicine.
```