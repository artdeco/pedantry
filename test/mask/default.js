import { makeTestSuite, deepEqual } from '@type.engineering/web-computing'
import { collect } from 'catchment'
import { sep } from 'path'
import Pedantry from '../../src'

export default makeTestSuite('test/result/default', {
  async getResults() {
    const [source, options] = this.input.split('\n')
    const opts = options ? JSON.parse(options) : {}
    const pedantry = new Pedantry({
      source,
      ...opts,
    })
    const files = []
    pedantry.on('file', f => files.push(f))
    const res = await collect(pedantry)
    return { res, files }
  },
  mapActual({ res }) {
    return res
  },
  assertResults({ files: actual }, { files }) {
    if (files) {
      deepEqual(actual, files.map((f) => f.replace(/\//g, sep)))
    }
  },
  splitRe: /^\/\/ /gm,
  jsonProps: ['files'],
})

export const options = makeTestSuite('test/result/options', {
  async getResults() {
    const pedantry = new Pedantry({
      source: this.input,
      ...this.options,
    })
    const files = []
    pedantry.on('file', f => files.push(f))
    const res = await collect(pedantry)
    return { res, files }
  },
  mapActual({ res }) {
    return res
  },
  assertResults({ files: actual }, { files }) {
    if (files) {
      deepEqual(actual, files.map((f) => f.replace(/\//g, sep)))
    }
  },
  jsonProps: ['files', 'options'],
})

export const ignore = makeTestSuite('test/result/ignore', {
  getReadable() {
    const p = new Pedantry({
      source: this.input,
      ...this.preamble,
    })
    return p
  },
  jsProps: ['preamble'],
})