import { Zoroaster, throws } from '@type.engineering/web-computing'
import { equal } from 'assert'
import { EOL } from 'os'
import { Transform } from 'stream'
// import Context from '../context'
import Pedantry from '../../src'

/** @type {Object.<string, (z: Zoroaster)>} */
const T = {
  context: [Zoroaster],
  'is a function'() {
    equal(typeof Pedantry, 'function')
  },
  async'emits an error event'() {
    const s = 'not-a-source'
    const pedantry = new Pedantry({
      source: s,
    })
    await throws({
      async fn() {
        await new Promise((r, j) => {
          pedantry.once('error', (err) => {
            j(err)
          })
        })
      },
      code: 'ENOENT',
    })
  },
  async'runs in object mode'({ snapshotExtension }) {
    snapshotExtension('md')
    const pedantry = new Pedantry({
      source: 'test/fixture/directory',
      includeFilename: true,
      addBlankLine: true,
      sep: '\n',
    })
    const data = []
    const o = new Transform({
      objectMode: true,
      transform(chunk, encoding, next) {
        data.push(chunk)
        next()
      },
    })
    pedantry.pipe(o)
    await new Promise((r, j) => {
      pedantry.on('end', () => {
        r()
      }).on('error', j)
    })
    // on win, test markdown files will have CRLF, otherwise LF, so CR?LF
    return data.map(({ file, data: d }) => {
      const f = file.replace(/\\/g, '/')
      d = d.replace(/\r?\n/g, (m, i, s) => `<CR?LF>${s[i+m.length] ? EOL : ''}`)
      return `# ${f}${EOL}${EOL}\`\`\`markdown${EOL}${d}${EOL}\`\`\``
    }).join(EOL)
  },
}

export default T