/** @record */
$_pedantry.IPedantrySymbolsIn = function() {}
/** @type {string|undefined} */
$_pedantry.IPedantrySymbolsIn.prototype._source
/** @type {string|undefined} */
$_pedantry.IPedantrySymbolsIn.prototype._file
/** @typedef {$_pedantry.IPedantrySymbolsIn} */
_pedantry.IPedantrySymbolsIn

/** @interface */
$_pedantry.IPedantrySymbolsOut = function() {}
/** @type {string} */
$_pedantry.IPedantrySymbolsOut.prototype._source
/** @type {string} */
$_pedantry.IPedantrySymbolsOut.prototype._file
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$_pedantry.IPedantrySymbolsOut}
 */
_pedantry.IPedantrySymbolsOut

/** @typedef {function(!_pedantry.IPedantry, !_pedantry.IPedantrySymbolsIn): void} */
_pedantry.setPedantrySymbols

/** @typedef {function(!_pedantry.IPedantry): !_pedantry.IPedantrySymbolsOut} */
_pedantry.getPedantrySymbols

