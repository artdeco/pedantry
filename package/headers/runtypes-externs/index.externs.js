/**
 * @fileoverview
 * @externs
 */

/**
 * @record
 * @extends {stream.TransformOptions}
 */
_pedantry.IPedantry.Initialese = function() {}
/** @type {boolean|undefined} */
_pedantry.IPedantry.Initialese.prototype.reverse
/** @type {boolean|undefined} */
_pedantry.IPedantry.Initialese.prototype.addNewLine
/** @type {string|undefined} */
_pedantry.IPedantry.Initialese.prototype.sep
/** @type {boolean|undefined} */
_pedantry.IPedantry.Initialese.prototype.addBlankLine
/** @type {boolean|undefined} */
_pedantry.IPedantry.Initialese.prototype.includeFilename
/** @type {boolean|undefined} */
_pedantry.IPedantry.Initialese.prototype.ignoreHidden
/** @type {(!Array<string>)|undefined} */
_pedantry.IPedantry.Initialese.prototype.extensions
/** @type {(!Array<string>)|undefined} */
_pedantry.IPedantry.Initialese.prototype.ignoreDirs
/** @type {string|undefined} */
_pedantry.IPedantry.Initialese.prototype.source

/** @interface */
_pedantry.IPedantryFields
/** @type {string} */
_pedantry.IPedantryFields.prototype.source
/** @type {string} */
_pedantry.IPedantryFields.prototype.file

/** @interface */
_pedantry.IPedantryCaster
/** @type {!_pedantry.BoundIPedantry} */
_pedantry.IPedantryCaster.prototype.asIPedantry
/** @type {!_pedantry.BoundPedantry} */
_pedantry.IPedantryCaster.prototype.superPedantry

/**
 * @interface
 * @extends {_pedantry.IPedantryFields}
 * @extends {engineering.type.IEngineer}
 * @extends {_pedantry.IPedantryCaster}
 */
_pedantry.IPedantry = function(init) {}

/**
 * @constructor
 * @implements {_pedantry.IPedantry}
 * @implements {engineering.type.IInitialiser<_pedantry.IPedantry, _pedantry.IPedantry.Initialese>}
 * @extends {stream.PassThrough}
 */
_pedantry.Pedantry = function(init) {}
/**
 * @param {...(!(_pedantry.IPedantry|typeof _pedantry.Pedantry)|(engineering.type.IInitialiser|typeof engineering.type.Initialiser)|(engineering.type.IEngineer|typeof engineering.type.Engineer))} Implementations
 * @return {typeof _pedantry.Pedantry}
 */
_pedantry.Pedantry.__implement = function(...Implementations) {}
/**
 * @param {...*} Extensions
 * @return {function(new: _pedantry.IPedantry)}
 */
_pedantry.Pedantry.__extend = function(...Extensions) {}

/** @typedef {typeof __$te_plain} */
_pedantry.RecordIPedantry

/**
 * @record
 * @extends {_pedantry.IPedantryFields}
 * @extends {_pedantry.RecordIPedantry}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {_pedantry.IPedantryCaster}
 */
_pedantry.BoundIPedantry = function() {}

/**
 * @record
 * @extends {_pedantry.BoundIPedantry}
 * @extends {engineering.type.BoundIInitialiser}
 */
_pedantry.BoundPedantry = function() {}

