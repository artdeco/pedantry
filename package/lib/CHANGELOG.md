## 15 December 2021

### [3.2.0](https://gitlab.com/artdeco/pedantry/compare/v3.1.1...v3.2.0)

- [Types] Add a type for object data to the externs.

## 14 September 2021

### [3.1.1](https://gitlab.com/artdeco/pedantry/compare/v3.1.0...v3.1.1)

- [Deps] Compile shared _Type Engineer_.

## 16 May 2021

### [3.1.0](https://gitlab.com/artdeco/pedantry/compare/v3.0.1-lib...v3.1.0)

- [Feature] Support the `extensions` option to only read files of certain
  extensions.
- [Fix] Add `type-engineer` to the compiled typedefs.
- [Fix] Let only the constructor extend _Writable_ but not the interface.

## 14 May 2021

### [3.0.1-lib](https://gitlab.com/artdeco/pedantry/compare/v3.0.0...v3.0.1-lib)

- [Fix] Correct the _type-engineer_ dependency version.

## 13 May 2021

### [3.0.0](https://gitlab.com/artdeco/pedantry/compare/v2.6.0...v3.0.0)

- [API] Change the _Pedantry_ API to use the initialiser and correctly extend
  the pass-through constructor by passing transform options to it.
- [Feature] Allow to specify the `ignoreDirs` to ignore folders by name.
- [Package] Publish on the _Luddites_ registry as `@artdeco/pedantry`.

## 7 April 2020

### [2.6.0](https://gitlab.com/artdeco/pedantry/compare/v2.5.2...v2.6.0)

- [license] Update to AGPL-3.0.
- [package] Move to _GitLab_.
- [fix] Write separator based on EOL.

## 3 February 2020

### [2.5.2](https://github.com/artdecocode/pedantry/compare/v2.5.1...v2.5.2)

- [deps] Move _Depack_ to `dev`.

### [2.5.1](https://github.com/artdecocode/pedantry/compare/v2.5.0...v2.5.1)

- [deps] Upgrade dependencies.

## 3 August 2019

### [2.5.0](https://github.com/artdecocode/pedantry/compare/v2.4.0...v2.5.0)

- [package] Compile with [_Depack_](https://compiler.page), add externs.

## 4 June 2019

### [2.4.0](https://github.com/artdecocode/pedantry/compare/v2.3.2...v2.4.0)

- [feature] Allow to ignore hidden files with the `ignoreHidden` option.
- [deps] Upgrade testing framework to [zoroaster](https://contexttesting.com)@4.

## 18 April 2019

### [2.3.2](https://github.com/artdecocode/pedantry/compare/v2.3.1...v2.3.2)

- [deps] Update and unlock dependencies.

## 28 March 2019

### [2.3.1](https://github.com/artdecocode/pedantry/compare/v2.3.0...v2.3.1)

- [fix] Fix nested include filename.

### [2.3.0](https://github.com/artdecocode/pedantry/compare/v2.2.0...v2.3.0)

- [feature] Push file path in the object mode with `includeFilename`.
- [package] Export the ES6 module.

## 14 September 2018

### 2.2.0

- [feature] Automatically insert `\n\n` with `addBlankLine` option.

### 2.1.0

- [feature] Automatically insert `\n` with `addNewLine` option.

### 2.0.0

- [feature] Sort files in numerical locale (**breaking**), implement reverse order and emit `file` events.
- [test] Write tests using a mask.

## 21 June 2018

### 1.0.1

- [feature] catch errors and emit error event

### 1.0.0

- Create `pedantry` with [`mnp`][https://mnpjs.org]
- [repository]: `src`, `test`
