import { createReadStream } from 'fs'
import { join } from 'path'
import { debuglog } from 'util'
import { setPedantrySymbols } from '../../types/lux'
import { getKeys, JUST_STARTED } from './'

const LOG = debuglog('pedantry')

/**
 * Processes a directory.
 * @param {!_pedantry.Pedantry} stream
 * @param {!Object} conf
 * @param {_pedantry.IPedantry.Initialese} [opts]
 */
export const processDir = async (stream, {
  source, path = '.', content = {}, separator,
}, opts) => {
  const{extensions,ignoreHidden,ignoreDirs=[],reverse,includeFilename}=opts || {}
  const k = Object.keys(content)
  const keys = getKeys(k, reverse)

  let size = 0
  for (const name of keys) {
    if (ignoreDirs.includes(name)) continue

    const { type, content: dirContent } = content[name]
    const relPath = join(path, name)

    let s
    if (type == 'File') {
      const shouldIgnore = ignoreHidden && name.startsWith('.')
      if (extensions) {
        if (!extensions.find((ext) => {
          return name.endsWith(ext)
        })) {
          continue
        }
      }
      if (shouldIgnore) continue
      s = await processFile(stream, {
        source, path: relPath, separator, includeFilename,
      })
    } else if (type == 'Directory') {
      s = await processDir(stream, {
        source, path: relPath, content: dirContent, separator,
      }, opts)
    }
    size += s
  }

  LOG('dir %s size: %s B', path, size)
  return size
}

/**
 * @param {!_pedantry.Pedantry} stream
 */
const processFile = async (stream, options) => {
  const {
    source, path, separator, includeFilename,
  } = options
  const fullPath = join(source, path)
  stream.emit('file', path)
  setPedantrySymbols(stream, {
    _file: fullPath,
  })
  if (separator && !stream[JUST_STARTED]) {
    if (includeFilename) {
      stream.push(/** @type {!_pedantry.ObjectModeData} */({ file: 'separator', data: separator }))
    } else {
      stream.push(separator)
    }
  }
  const size = await new Promise((r, j) => {
    let s = 0
    const rs = createReadStream(fullPath)
    rs.on('data', (d) => {
      s += d.byteLength
    }).on('error', (err) => {
      j(err)
    }).on('close', () => {
      r(s)
    })
    if (includeFilename) {
      rs.on('data', (data) => {
        stream.push(/** @type {!_pedantry.ObjectModeData} */ ({ file: fullPath, data: `${data}` }))
      })
    } else {
      rs.pipe(stream, { end: false })
    }
  })
  stream[JUST_STARTED] = false
  LOG('file %s :: %s B', fullPath, size)
  return size
}
