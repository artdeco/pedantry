import readDirStructure from '@wrote/read-dir-structure'
import { EOL } from 'os'
import '../../types'
import AbstractPedantry, { setPedantrySymbols } from '../../types/lux'
import { JUST_STARTED } from './'
import { processDir } from './read'

export default class Pedantry extends AbstractPedantry {
  constructor(init = {}) {
    const Init = /** @type {!_pedantry.IPedantry.Initialese} */ (init)
    const opts = /** @type {!_pedantry.IPedantry.Initialese} */ ({
      ...Init,
      defaultEncoding: true,
      objectMode: init.includeFilename,
    })
    super(opts)
    this.initialiser(opts)
  }
}

/** @constructor @extends {AbstractPedantry} @suppress {checkTypes} */ function _Pedantry() {}
AbstractPedantry.__implement(
  _Pedantry.prototype = /** @type {!AbstractPedantry} */ ({
    initialiser(opts) {
      const {
        source,
        addNewLine = false,
        addBlankLine = false,
        sep = EOL,
      } = opts || {}
      setPedantrySymbols(this, {
        _source: source,
      })
      let separator
      if (addNewLine) separator = sep
      else if (addBlankLine) separator = `${sep}${sep}`
      this[JUST_STARTED] = true
      ;(async () => {
        let content
        try {
          ({ content } = await readDirStructure(/** @type {string} */ (source)))
        } catch (err) {
          const e = new Error(err.message)
          e['code'] = err['code']
          this.emit('error', e)
        }
        try {
          await processDir(this, {
            source,
            content,
            separator,
          }, opts)
        } catch (err) {
          this.emit('error', err)
        } finally {
          this.end()
        }
      })()
      return this
    },
  }),
)