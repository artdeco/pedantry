import { LIB as lib } from '@artdeco/package'
import '@luddites/lud'
import { readFileSync, mkdirSync, writeFileSync, rmSync } from 'fs'
import { dependencies, description, name } from '../package.json'
import { TYPEDEFS } from './common'

export const LIB = lib(description, dependencies)
LIB.addFiles = LIB.addFiles.filter((f) => f != 'types')
LIB.addFiles.push('types/lux/')
LIB.ignore.push('src/chunks.json', 'src/pedantry.js',
  // 'types/typedefs/private.typal.js' // this ignore doesn't work
)
delete LIB.dependencies['@type.engineering/type-engineer']
delete LIB.dependencies['@type.engineering/type-engineer.h']
LIB.peerDependencies = {
  '@type.engineering/type-engineer': '*',
  '@type.engineering/type-engineer.h': '*',
}
// LIB.addFiles.push(LISTENER)
// LIB.copy = LISTENER


LIB.postProcessing = async () => {
  console.log('postprocessing lib')
  rmSync('package/lib/types/lux/typal.js')
  try {
    mkdirSync('package/lib/types')
  } catch (err) {}
  try {
    mkdirSync('package/lib/types/typedefs')
  } catch (err) {}
  writeFileSync('package/lib/types/index.js', `import '${name}.h'`)
  writeFileSync('package/lib/types/typedefs/private.js', ``)
  const INDEX = 'package/lib/index.js'
  let r = readFileSync(INDEX, 'utf-8')
  for (const k in TYPEDEFS) {
    const v = TYPEDEFS[k].replace('.js', '')
    r = r.replace(`./${v}`, k.replace('node_modules/', '').replace(/\/index\.js/, ''))
  }
  r = r.replace('./types/typedefs', `${name}.h/typedefs`)
  // r = r.replace('./types/jsx.typedefs', '@a-la2/jsx/types/typedefs')
  writeFileSync(INDEX, r)
}
LIB.copy = {}
delete LIB.runtypes