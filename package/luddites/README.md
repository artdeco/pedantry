<div align="center">

# pedantry

[![npm version](https://badge.fury.io/js/pedantry.svg)](https://www.npmjs.com/package/pedantry)
<a href="https://gitlab.com/artdeco/pedantry/-/commits/master">
  <img src="https://gitlab.com/artdeco/pedantry/badges/master/pipeline.svg" alt="Pipeline Badge">
</a>
</div>

_Pedantry_ is a readable stream that puts together all files and nested directories in the given directory in sorted order (`1.md`, `2.md`, `3/1.md`, `3/1.5.md`, `10.md`, _etc_). It will also read `index.md` and `footer.md` as first and last files respectively if found.

```sh
yarn add pedantry
npm i pedantry
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [API](#api)
- [class `Pedantry`](#class-pedantry)
  * [`Pedantry`](#type-pedantry)
  * [`Options`](#type-options)
  * [Reverse Order](#reverse-order)
  * [Events](#events)
  * [Object Mode](#object-mode)
- [Ignore Hidden](#ignore-hidden)
- [Copyright & License](#copyright--license)

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/1.svg?sanitize=true">
</a></div>


## API

The main export of the program is the `Pedantry` duplex stream which should only be used as a Readable.

```js
import Pedantry from 'pedantry'
```


## class `Pedantry`

A _Pedantry_ instance reads files in order one-by-one and pushes the results further down the pipe.

__<a name="type-pedantry">`Pedantry`</a>__
<table>
 <thead><tr>
  <th>Name</th>
  <th>Type &amp; Description</th>
 </tr></thead>
 <tr>
  <td rowSpan="3" align="center"><ins>constructor</ins></td>
  <td><em>new (source: string, options?: <a href="#type-options" title="Options for Pedantry.">!Options</a>) => <a href="#type-pedantry">Pedantry</a></em></td>
 </tr>
 <tr></tr>
 <tr>
  <td>
   Constructor method.<br/>
   <kbd><strong>source*</strong></kbd> <em><code>string</code></em>: The path to the root directory.<br/>
   <kbd>options</kbd> <em><code><a href="#type-options" title="Options for Pedantry.">!Options</a></code></em> (optional): Options for Pedantry.
  </td>
 </tr>
</table>

Create a new readable stream. Upon creation, `Pedantry` will start reading files in the `source` directory recursively in the following order:

1. the content of the `index.md` file will go first if it exists,
1. then of all files and directories in the folder recursively in a sorted order (possibly in reverse),
1. and the content of the `footer.md` file will go last if found.

__<a name="type-options">`Options`</a>__: Options for Pedantry.


|      Name       |       Type       |                                                                                            Description                                                                                            | Default  |
| --------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| reverse         | <em>boolean</em> | Whether to print files in reverse order, i.e., `30-file.md` before `1-file.md`.                                                                                                                   | `false`  |
| addNewLine      | <em>boolean</em> | Add a new line separator (`\n`, `\r\n` on Windows, or `sep` from options) between the content of each file.                                                                                       | `false`  |
| sep             | <em>string</em>  | The separator to use between files.                                                                                                                                                               | `os.EOL` |
| addBlankLine    | <em>boolean</em> | Add a blank line between the content of each file, which is equivalent to inserting `\n\n`.                                                                                                       | `false`  |
| includeFilename | <em>boolean</em> | When this is set to `true`, _Pedantry_ will write data in object mode, pushing an object with `file` and `data` properties. New and blank lines will have the `file` property set to `separator`. | `false`  |
| ignoreHidden    | <em>boolean</em> | Don't read files that start with the `.` symbol.                                                                                                                                                  | `false`  |

_Given the directory structure:_

```m
example/test
├── 1-words.md
├── 11-to-live.md
├── 2-believe.md
├── 3-brick.md
├── footer.md
└── index.md
```

_The usage of **Pedantry** is as below:_

```js
import Pedantry from 'pedantry'

const pedantry = new Pedantry('example/test')
pedantry.pipe(process.stdout)
```
```markdown
# index.md: Welcome to Quotes

This is a collection of quotes.

## 1-words.md: Mikhail Bulgakov, The Master and Margarita

“You pronounced your words as if you don’t acknowledge the shadows, or the evil
either. Would you be so kind as to give a little thought to the question of what
your good would be doing if evil did not exist, and how the earth would look if
the shadows were to disappear from it?”

## 2-believe.md: Mikhail Bulgakov, The Master and Margarita

“I believe you!' the artiste exclaimed finally and extinguishes his gaze. 'I do!
These eyes are not lying! How many times have I told you that your basic error
consists in underestimating the significance of the human eye. Understand that
the tongue can conceal the truth, but the eyes - never! A sudden question is put
to you, you don't even flinch, in one second you get hold of yourself and know
what you must say to conceal the truth, and you speak quite convincingly, and
not a wrinkle on your face moves, but - alas - the truth which the question
stirs up from the bottom of your soul leaps momentarily into your eyes, and it's
all over! They see it, and you're caught!”

## 3-brick.md: Mikhail Bulgakov, The Master and Margarita

“The brick is neither here nor there,' interrupted the stranger in an imposing
fashion, 'it never merely falls on someone's head from out of nowhere. In your
case, I can assure you that a brick poses no threat whatsoever. You will die
another kind of death."

'And you know just what that will be?' queried Berlioz with perfectly
understandable irony, letting himself be drawn into a truly absurd conversation.
'And can you tell me what that is?'

'Gladly,' replied the stranger. He took Berlioz's measure as if intending to
make him a suit and muttered something through his teeth that sounded like 'One,
two.. Mercury in the Second House... the moon has set... six-misfortune...
evening-seven...' Then he announced loudly and joyously, 'Your head will be cut
off!”

## 11-to-live.md: Friedrich Nietzsche

> To live is to suffer, to survive is to find some meaning in the suffering.

## footer.md: Copyright

[source](https://www.goodreads.com/work/quotes/876183?page=2)
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/2.svg?sanitize=true" width="15">
</a></div>

### Reverse Order

To print in reverse order, the `reverse` option can be set. This feature could be useful when writing a blog, for example, as date 23 will follow 22, and in the output it will be printed first.

_With a simpler directory structure:_

```m
example/simple-test
├── 1-file.md
├── 100-world.md
├── 2-test.md
├── 21-hello.md
├── footer.md
└── index.md
```

_It could be printed in reverse:_

```js
import Pedantry from 'pedantry'

const pedantry = new Pedantry('example/simple-test', {
  reverse: true,
})
pedantry.pipe(process.stdout)
```
```markdown
index.md
100-world.md
21-hello.md
2-test.md
1-file.md
footer.md
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/3.svg?sanitize=true" width="15">
</a></div>

### Events

The _Pedantry_ stream will emit `file` events when a file is started to be read. The content of this event is the path to the currently read file relative to the source directory.

```js
import Pedantry from 'pedantry'

const pedantry = new Pedantry('example/simple-test')
pedantry.on('file', f => console.log(f))
```
```fs
index.md
1-file.md
2-test.md
21-hello.md
100-world.md
footer.md
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/4.svg?sanitize=true" width="15">
</a></div>

### Object Mode

To get access to the currently processed file, _Pedantry_ can be run in object mode, in which it will emit the `data` event with an object consisting of `file` and `data` properties. If blank lines are added, their will be reported as coming from the `separator` file.

```js
import Pedantry from 'pedantry'
import { Transform } from 'stream'

const pedantry = new Pedantry('example/simple-test', {
  includeFilename: true,
  addBlankLine: true,
})
const t = new Transform({
  objectMode: true,
  transform(object, _, next) {
    console.log(object)
    next()
  },
})
pedantry.pipe(t)
```
```fs
{ file: 'example/simple-test/index.md', data: 'index.md\n' }
{ file: 'separator', data: '\n\n' }
{ file: 'example/simple-test/1-file.md', data: '1-file.md\n' }
{ file: 'separator', data: '\n\n' }
{ file: 'example/simple-test/2-test.md', data: '2-test.md\n' }
{ file: 'separator', data: '\n\n' }
{ file: 'example/simple-test/21-hello.md', data: '21-hello.md\n' }
{ file: 'separator', data: '\n\n' }
{ file: 'example/simple-test/100-world.md', data: '100-world.md\n' }
{ file: 'separator', data: '\n\n' }
{ file: 'example/simple-test/footer.md', data: 'footer.md' }
```

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/5.svg?sanitize=true" width="15">
</a></div>

## Ignore Hidden

To ignore hidden files, the `{ ignore: hidden }` option can be passed to _Pedantry_. By default, this is set to false.

<table>
<tr><th><a href="example/hidden.js">The Source </a></th><th><a href="example/hidden">The Tree</a></th></tr>
<tr><td>

```js
import Pedantry from 'pedantry'

const HIDDEN = 'example/hidden'
const pedantry = new Pedantry(HIDDEN, {
  ignoreHidden: true,
})
pedantry.pipe(process.stdout)
```
</td>
<td>

```m
example/hidden
├── .ignore
└── 1.md
```
</td></tr>
<tr><td colspan="2" align="center"><strong>The Output</strong></td></tr>
<tr><td colspan="2">

```fs
1.md: hello world
```
</td></tr>
</table>


<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/6.svg?sanitize=true">
</a></div>

## Copyright & License

GNU Affero General Public License v3.0

<table>
  <tr>
    <th>
      <a href="https://www.artd.eco">
        <img width="100" src="https://gitlab.com/uploads/-/system/group/avatar/7454762/artdeco.png"
          alt="Art Deco">
      </a>
    </th>
    <th>© <a href="https://www.artd.eco">Art Deco™</a>   2020</th>
    <th><a href="LICENSE"><img src=".documentary/agpl-3.0.svg" alt="AGPL-3.0"></a></th>
  </tr>
</table>

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/-1.svg?sanitize=true">
</a></div>