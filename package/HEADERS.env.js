import '@luddites/lud'
import { readFileSync, writeFileSync } from 'fs'
import { description, name } from '../package.json'
import { importsToRequire, removeTypalMeta } from '@artdeco/package'

/** @type {_lud.Env} */
export const HEADERS = {
  name: name + '.h',
  ignore: [
    '**/typal.js',
    '**/*.typal.js',
    '**/private/*',
  ],
  module: '_.js',
  description: `${description} [HEADERS]`,
  runtypes: [
    'runtypes-externs/ns/$_pedantry.ns',
    // 'runtypes-externs/ns/$guest.maurice.ns',
    // 'runtypes-externs/ns/$guest.maurice.ui.ns',
    'runtypes-externs/ns/__$te.ns',
    'runtypes-externs/index.js',
    // 'runtypes-externs/api.js',
    'runtypes-externs/symbols.js',
  ],
  Externs: [
    'runtypes-externs/index.externs.js',
  ],
  externs: [
    'runtypes-externs/ns/_pedantry.js',
    'runtypes-externs/externs.js',
  ],
  addFiles: [
    'types/typedefs',
    'types/runtypes-externs',

    'types/index.js',
    'node_modules/@artdeco/license/EULA.md',
    'CHANGELOG.md',
  ],
}

HEADERS.postProcessing = async () => {
  console.log('Postprocessing types')
  // renameVendor('headers')

  writeFileSync('package/headers/_.js', '') // module isn't required

  removeTypalMeta('package/headers',
    'runtypes-externs/externs.js',
    'runtypes-externs/index.externs.js', 'runtypes-externs/index.js',
    'typedefs/index.js', 'runtypes-externs/symbols.js')

  importsToRequire('package/headers/index.js')
}