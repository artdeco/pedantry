/** @nocompile */
/** @const */
var _pedantry = {}

/**
 * @typedef {Object} $_pedantry.IPedantry.Initialese
 * @prop {boolean} [reverse=false] Whether to print files in reverse order, i.e., `30-file.md` before
 * `1-file.md`. Default `false`.
 * @prop {boolean} [addNewLine=false] Add a new line separator (`\n`, `\r\n` on Windows, or `sep` from
 * options) between the content of each file. Default `false`.
 * @prop {string} [sep="os.EOL"] The separator to use between files. Default `os.EOL`.
 * @prop {boolean} [addBlankLine=false] Add a blank line between the content of each file, which is equivalent
 * to inserting `\n\n`. Default `false`.
 * @prop {boolean} [includeFilename=false] When this is set to `true`, _Pedantry_ will write data in object mode,
 * pushing an object with `file` and `data` properties. New and blank lines
 * will have the `file` property set to `separator`. Default `false`.
 * @prop {boolean} [ignoreHidden=false] Don't read files that start with the `.` symbol. Default `false`.
 * @prop {!Array<string>} [extensions] A list of extensions to read. When not given, all files will be read.
 * @prop {!Array<string>} [ignoreDirs] The files inside any of these folders will be ignored, e.g.,
 * `[.cache]` will ignore the cache directory anywhere inside the source
 * folder. Default `[]`.
 * @prop {string} [source] The path to the root directory.
 */
/** @typedef {$_pedantry.IPedantry.Initialese&stream.TransformOptions} _pedantry.IPedantry.Initialese Options for Pedantry. */

/** @typedef {function(new: _pedantry.IPedantryFields&engineering.type.IEngineer&_pedantry.IPedantryCaster)} _pedantry.IPedantry.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * An interface that allows to read directories as streams.
 * Upon creation, `Pedantry` will start reading files in the `source` directory
 * recursively in the following order: the content of the `index.md` file will
 * go first, then of all files and directories in the folder recursively in a
 * sorted order, and the content of the `footer.md` file will go last if found.
 * @interface _pedantry.IPedantry
 */
_pedantry.IPedantry = class extends /** @type {_pedantry.IPedantry.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  /** @param {!_pedantry.IPedantry.Initialese} [init] */
  constructor(init) {
    super(init)
  }
}
_pedantry.IPedantry.prototype.constructor = _pedantry.IPedantry

/** @typedef {function(new: stream.PassThrough&_pedantry.IPedantry&engineering.type.IInitialiser<_pedantry.IPedantry, _pedantry.IPedantry.Initialese>)} _pedantry.Pedantry.constructor */
/** @typedef {typeof _pedantry.IPedantry} _pedantry.IPedantry.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IPedantry_ instances.
 * @constructor _pedantry.Pedantry
 * @implements {_pedantry.IPedantry} An interface that allows to read directories as streams.
 * Upon creation, `Pedantry` will start reading files in the `source` directory
 * recursively in the following order: the content of the `index.md` file will
 * go first, then of all files and directories in the folder recursively in a
 * sorted order, and the content of the `footer.md` file will go last if found.
 * @implements {engineering.type.IInitialiser<_pedantry.IPedantry, _pedantry.IPedantry.Initialese>} ‎
 */
_pedantry.Pedantry = class extends /** @type {_pedantry.Pedantry.constructor&_pedantry.IPedantry.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Creates a new _Pedantry_.
   * @param {!_pedantry.IPedantry.Initialese} [init] Initialisation options.
   * - `[reverse=false]` _boolean?_ Whether to print files in reverse order, i.e., `30-file.md` before
   * `1-file.md`. Default `false`.
   * - `[addNewLine=false]` _boolean?_ Add a new line separator (`\n`, `\r\n` on Windows, or `sep` from
   * options) between the content of each file. Default `false`.
   * - `[sep="os.EOL"]` _string?_ The separator to use between files. Default `os.EOL`.
   * - `[addBlankLine=false]` _boolean?_ Add a blank line between the content of each file, which is equivalent
   * to inserting `\n\n`. Default `false`.
   * - `[includeFilename=false]` _boolean?_ When this is set to `true`, _Pedantry_ will write data in object mode,
   * pushing an object with `file` and `data` properties. New and blank lines
   * will have the `file` property set to `separator`. Default `false`.
   * - `[ignoreHidden=false]` _boolean?_ Don't read files that start with the `.` symbol. Default `false`.
   * - `[extensions]` _!Array&lt;string&gt;?_ A list of extensions to read. When not given, all files will be read.
   * - `[ignoreDirs]` _!Array&lt;string&gt;?_ The files inside any of these folders will be ignored, e.g.,
   * `[.cache]` will ignore the cache directory anywhere inside the source
   * folder. Default `[]`.
   * - `[source]` _string?_ The path to the root directory.
   */
  constructor(init) {
    super(init)
  }
}
_pedantry.Pedantry.prototype.constructor = _pedantry.Pedantry
/**
 * Adds implementations to the abstract class.
 * @param {...(!(_pedantry.IPedantry|typeof _pedantry.Pedantry)|(engineering.type.IInitialiser|typeof engineering.type.Initialiser)|(engineering.type.IEngineer|typeof engineering.type.Engineer))} Implementations
 */
_pedantry.Pedantry.__implement = function(...Implementations) {}

/**
 * Fields of the IPedantry.
 * @interface _pedantry.IPedantryFields
 */
_pedantry.IPedantryFields = class { }
/**
 * The path to the root directory. Default empty string.
 * @type {string}
 */
_pedantry.IPedantryFields.prototype.source
/**
 * The current file that is being processed. Default empty string.
 * @type {string}
 */
_pedantry.IPedantryFields.prototype.file

/** @typedef {_pedantry.IPedantry} */
_pedantry.RecordIPedantry

/** @typedef {_pedantry.IPedantry} _pedantry.BoundIPedantry */

/** @typedef {_pedantry.Pedantry} _pedantry.BoundPedantry */

/**
 * Contains getters to cast the _IPedantry_ interface.
 * @interface _pedantry.IPedantryCaster
 */
_pedantry.IPedantryCaster = class { }
/**
 * Cast the _IPedantry_ instance into the _BoundIPedantry_ type.
 * @type {!_pedantry.BoundIPedantry}
 */
_pedantry.IPedantryCaster.prototype.asIPedantry
/**
 * Access the _Pedantry_ prototype.
 * @type {!_pedantry.BoundPedantry}
 */
_pedantry.IPedantryCaster.prototype.superPedantry

/**
 * @typedef {Object} _pedantry.ObjectModeData An external interface for data in object mode with the indication of the
 * file which contents has just been read.
 * @prop {string} file The file which has been read.
 * @prop {string} data The contents of the file.
 */

const stream = {}
/** @typedef {import('stream').PassThrough} */
stream.PassThrough
/** @typedef {import('stream').TransformOptions} */
stream.TransformOptions