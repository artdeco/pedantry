const { Pedantry: _Pedantry } = require('./compile/pedantry')
require('@type.engineering/type-engineer/types/typedefs')
require('./types/typedefs')

/**
 * An interface that allows to read directories as streams.
 * Upon creation, `Pedantry` will start reading files in the `source` directory
 * recursively in the following order: the content of the `index.md` file will
 * go first, then of all files and directories in the folder recursively in a
 * sorted order, and the content of the `footer.md` file will go last if found.
 * @extends {_pedantry.Pedantry}
 */
class Pedantry extends _Pedantry {}

module.exports = Pedantry
