import { Pedantry } from './compile/pedantry' /* compiler renameReport:../rename-report.txt */
require('@type.engineering/type-engineer/types/typedefs')
require('./types/typedefs')

/** @api {_pedantry.Pedantry} */
export default Pedantry
