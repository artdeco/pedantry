/**
 * @fileoverview
 * @typedefs
 */

/* typal types/design */

const stream = {}
/** @typedef {import('stream').PassThrough} */
stream.PassThrough
/** @typedef {import('stream').TransformOptions} */
stream.TransformOptions