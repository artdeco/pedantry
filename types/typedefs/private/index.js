/** @nocompile */
/** @const */
var _pedantry = {}

/* @typal-start {types/design/IPedantry.xml} filter:Symbolism 433d000a000e2ad67709802753e4d17e */
/**
 * @typedef {Object} _pedantry.IPedantrySymbolsIn
 * @prop {string} [_source] The path to the root directory.
 * @prop {string} [_file] The current file that is being processed.
 */

/** @interface _pedantry.IPedantrySymbolsOut */
_pedantry.IPedantrySymbolsOut = class {
  constructor() {
    /**
     * The path to the root directory.
     */
    this._source = /** @type {string} */ (void 0)
    /**
     * The current file that is being processed.
     */
    this._file = /** @type {string} */ (void 0)
  }
}
_pedantry.IPedantrySymbolsOut.prototype.constructor = _pedantry.IPedantrySymbolsOut

/** @typedef {typeof _pedantry.setPedantrySymbols} */
/**
 * Sets the symbols on the IPedantry instance.
 * @param {!_pedantry.IPedantry} instance An _IPedantry_ instance on which to set symbols.
 * @param {!_pedantry.IPedantrySymbolsIn} symbols The symbols.
 * - `[_source]` _string?_ The path to the root directory.
 * - `[_file]` _string?_ The current file that is being processed.
 * @return {void}
 */
_pedantry.setPedantrySymbols = function(instance, symbols) {}

/** @typedef {typeof _pedantry.getPedantrySymbols} */
/**
 * Gets the symbols on the IPedantry instance.
 * @param {!_pedantry.IPedantry} instance An _IPedantry_ instance from which to read symbols.
 * @return {!_pedantry.IPedantrySymbolsOut} A map of private properties installed as symbols on the instance and their values.
 */
_pedantry.getPedantrySymbols = function(instance) {}

// nss:_pedantry
/* @typal-end */
