import { PassThrough } from 'stream'
import '../'
import '../typedefs/private'

/** @abstract {_pedantry.IPedantry} */
export default class AbstractPedantry extends PassThrough {}