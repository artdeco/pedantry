import { create, iu, bindPrototype, initAbstract, makeClassSymbol, getSymbols, setSymbols } from '@type.engineering/type-engineer'
import { PassThrough } from 'stream'
import '../'
import '../typedefs/private'

/**
 * @abstract
 * @extends {_pedantry.Pedantry}
 */
class _AbstractPedantry extends /** @type {typeof AbstractPedantry} */ (PassThrough) {
  /** @param {!_pedantry.IPedantry.Initialese} [init] */
  constructor(init) {
    super(init)
    create(this, arguments)
  }
  /** @return {void} */
  __$constructor() {
    if (PassThrough.prototype.__$constructor) try { PassThrough.prototype.__$constructor.apply(this, arguments) } catch (e) { /**/ }
    const { _source, _file } = getPedantrySymbols(this)
    setPedantrySymbols(this, {
      _source: iu(_source, ''),
      _file: iu(_file, ''),
    })
  }
  get source() {
    return this[PedantrySymbols._source]
  }
  get file() {
    return this[PedantrySymbols._file]
  }
  get asIPedantry() {
    return /** @type {!_pedantry.BoundIPedantry} */ (this)
  }
  get superPedantry() {
    return /** @type {!_pedantry.BoundPedantry} */ (bindPrototype(_AbstractPedantry, this))
  }
}
/**
 * An interface that allows to read directories as streams.
 * Upon creation, `Pedantry` will start reading files in the `source` directory
 * recursively in the following order: the content of the `index.md` file will
 * go first, then of all files and directories in the folder recursively in a
 * sorted order, and the content of the `footer.md` file will go last if found.
 * @extends {_pedantry.Pedantry} ‎
 * @implements {_pedantry.IPedantry} ‎
 * @implements {engineering.type.IInitialiser<_pedantry.IPedantry, _pedantry.IPedantry.Initialese>} ‎
 */
export default class AbstractPedantry extends _AbstractPedantry {

}
initAbstract(_AbstractPedantry, 'IPedantry')
export const PedantrySymbols = {
  _source: makeClassSymbol(AbstractPedantry, '_source'),
  _file: makeClassSymbol(AbstractPedantry, '_file'),
}

/** @type {_pedantry.getPedantrySymbols} */
export const getPedantrySymbols = (instance) => {
  return /** @type {!_pedantry.IPedantrySymbolsOut} */ (getSymbols(instance, PedantrySymbols))
}
/** @type {_pedantry.setPedantrySymbols} */
export const setPedantrySymbols = (instance, symbols) => {
  setSymbols(instance, symbols, PedantrySymbols)
}

/** @constructor @extends {AbstractPedantry} @suppress {checkTypes} */
AbstractPedantry.class = function() {}