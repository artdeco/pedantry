/**
 * @fileoverview
 * @externs
 */

/* @typal-type {types/design/ObjectModeData.xml} _pedantry.ObjectModeData  71e9ec391f050cdc27a00b7984cf316a */
/** @typedef {{ file: string, data: string }} */
_pedantry.ObjectModeData

// nss:_pedantry
/* @typal-end */

// any other externs