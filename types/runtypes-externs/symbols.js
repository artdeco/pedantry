/* @typal-type {types/design/IPedantry.xml} _pedantry.IPedantrySymbolsIn skipNsDecl filter:Symbolism 433d000a000e2ad67709802753e4d17e */
/** @record */
$_pedantry.IPedantrySymbolsIn = function() {}
/** @type {string|undefined} */
$_pedantry.IPedantrySymbolsIn.prototype._source
/** @type {string|undefined} */
$_pedantry.IPedantrySymbolsIn.prototype._file
/** @typedef {$_pedantry.IPedantrySymbolsIn} */
_pedantry.IPedantrySymbolsIn

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.IPedantrySymbolsOut skipNsDecl filter:Symbolism 433d000a000e2ad67709802753e4d17e */
/** @interface */
$_pedantry.IPedantrySymbolsOut = function() {}
/** @type {string} */
$_pedantry.IPedantrySymbolsOut.prototype._source
/** @type {string} */
$_pedantry.IPedantrySymbolsOut.prototype._file
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$_pedantry.IPedantrySymbolsOut}
 */
_pedantry.IPedantrySymbolsOut

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.setPedantrySymbols skipNsDecl filter:Symbolism 433d000a000e2ad67709802753e4d17e */
/** @typedef {function(!_pedantry.IPedantry, !_pedantry.IPedantrySymbolsIn): void} */
_pedantry.setPedantrySymbols

// nss:_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.getPedantrySymbols skipNsDecl filter:Symbolism 433d000a000e2ad67709802753e4d17e */
/** @typedef {function(!_pedantry.IPedantry): !_pedantry.IPedantrySymbolsOut} */
_pedantry.getPedantrySymbols

// nss:_pedantry
/* @typal-end */
