/* @typal-type {types/design/IPedantry.xml} _pedantry.IPedantry.Initialese  433d000a000e2ad67709802753e4d17e */
/**
 * @record
 * @extends {stream.TransformOptions}
 */
$_pedantry.IPedantry.Initialese = function() {}
/** @type {boolean|undefined} */
$_pedantry.IPedantry.Initialese.prototype.reverse
/** @type {boolean|undefined} */
$_pedantry.IPedantry.Initialese.prototype.addNewLine
/** @type {string|undefined} */
$_pedantry.IPedantry.Initialese.prototype.sep
/** @type {boolean|undefined} */
$_pedantry.IPedantry.Initialese.prototype.addBlankLine
/** @type {boolean|undefined} */
$_pedantry.IPedantry.Initialese.prototype.includeFilename
/** @type {boolean|undefined} */
$_pedantry.IPedantry.Initialese.prototype.ignoreHidden
/** @type {(!Array<string>)|undefined} */
$_pedantry.IPedantry.Initialese.prototype.extensions
/** @type {(!Array<string>)|undefined} */
$_pedantry.IPedantry.Initialese.prototype.ignoreDirs
/** @type {string|undefined} */
$_pedantry.IPedantry.Initialese.prototype.source
/** @typedef {$_pedantry.IPedantry.Initialese} */
_pedantry.IPedantry.Initialese

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.IPedantryFields  433d000a000e2ad67709802753e4d17e */
/** @interface */
$_pedantry.IPedantryFields = function() {}
/** @type {string} */
$_pedantry.IPedantryFields.prototype.source
/** @type {string} */
$_pedantry.IPedantryFields.prototype.file
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$_pedantry.IPedantryFields}
 */
_pedantry.IPedantryFields

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.IPedantryCaster  433d000a000e2ad67709802753e4d17e */
/** @interface */
$_pedantry.IPedantryCaster = function() {}
/** @type {!_pedantry.BoundIPedantry} */
$_pedantry.IPedantryCaster.prototype.asIPedantry
/** @type {!_pedantry.BoundPedantry} */
$_pedantry.IPedantryCaster.prototype.superPedantry
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$_pedantry.IPedantryCaster}
 */
_pedantry.IPedantryCaster

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.IPedantry  433d000a000e2ad67709802753e4d17e */
/**
 * @interface
 * @extends {_pedantry.IPedantryFields}
 * @extends {engineering.type.IEngineer}
 * @extends {_pedantry.IPedantryCaster}
 */
$_pedantry.IPedantry = function(init) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$_pedantry.IPedantry}
 */
_pedantry.IPedantry

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.Pedantry  433d000a000e2ad67709802753e4d17e */
/**
 * @constructor
 * @implements {_pedantry.IPedantry}
 * @implements {engineering.type.IInitialiser<_pedantry.IPedantry, _pedantry.IPedantry.Initialese>}
 * @extends {stream.PassThrough}
 */
$_pedantry.Pedantry = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {!_pedantry.IPedantry.Initialese} [init] Initialisation options.
 * - `[reverse=false]` _boolean?_ Whether to print files in reverse order, i.e., `30-file.md` before
 * `1-file.md`. Default `false`.
 * - `[addNewLine=false]` _boolean?_ Add a new line separator (`\n`, `\r\n` on Windows, or `sep` from
 * options) between the content of each file. Default `false`.
 * - `[sep="os.EOL"]` _string?_ The separator to use between files. Default `os.EOL`.
 * - `[addBlankLine=false]` _boolean?_ Add a blank line between the content of each file, which is equivalent
 * to inserting `\n\n`. Default `false`.
 * - `[includeFilename=false]` _boolean?_ When this is set to `true`, _Pedantry_ will write data in object mode,
 * pushing an object with `file` and `data` properties. New and blank lines
 * will have the `file` property set to `separator`. Default `false`.
 * - `[ignoreHidden=false]` _boolean?_ Don't read files that start with the `.` symbol. Default `false`.
 * - `[extensions]` _!Array&lt;string&gt;?_ A list of extensions to read. When not given, all files will be read.
 * - `[ignoreDirs]` _!Array&lt;string&gt;?_ The files inside any of these folders will be ignored, e.g.,
 * `[.cache]` will ignore the cache directory anywhere inside the source
 * folder. Default `[]`.
 * - `[source]` _string?_ The path to the root directory.
 * @extends {$_pedantry.Pedantry}
 */
_pedantry.Pedantry
/**
 * @param {...(!(_pedantry.IPedantry|typeof _pedantry.Pedantry)|(engineering.type.IInitialiser|typeof engineering.type.Initialiser)|(engineering.type.IEngineer|typeof engineering.type.Engineer))} Implementations
 * @return {typeof _pedantry.Pedantry}
 */
_pedantry.Pedantry.__implement
/**
 * @param {...*} Extensions
 * @return {function(new: _pedantry.IPedantry)}
 */
_pedantry.Pedantry.__extend

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.RecordIPedantry  433d000a000e2ad67709802753e4d17e */
/** @typedef {typeof __$te_plain} */
_pedantry.RecordIPedantry

// nss:_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.BoundIPedantry  433d000a000e2ad67709802753e4d17e */
/**
 * @record
 * @extends {_pedantry.IPedantryFields}
 * @extends {_pedantry.RecordIPedantry}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {_pedantry.IPedantryCaster}
 */
$_pedantry.BoundIPedantry = function() {}
/** @typedef {$_pedantry.BoundIPedantry} */
_pedantry.BoundIPedantry

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/IPedantry.xml} _pedantry.BoundPedantry  433d000a000e2ad67709802753e4d17e */
/**
 * @record
 * @extends {_pedantry.BoundIPedantry}
 * @extends {engineering.type.BoundIInitialiser}
 */
$_pedantry.BoundPedantry = function() {}
/** @typedef {$_pedantry.BoundPedantry} */
_pedantry.BoundPedantry

// nss:_pedantry,$_pedantry
/* @typal-end */
/* @typal-type {types/design/ObjectModeData.xml} _pedantry.ObjectModeData  71e9ec391f050cdc27a00b7984cf316a */
/** @typedef {{ file: string, data: string }} */
_pedantry.ObjectModeData

// nss:_pedantry
/* @typal-end */
